class Board

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def count
    ship_count = 0
    @grid.each do |row|
      row.each do |el|
        ship_count += 1 if el == :s
      end
    end
    ship_count
  end

  def empty?(*pos)
    if pos == [] && count < 1
      return true
    elsif pos == [] && count > 0
      return false
    else
      row, el = pos[0]
      if @grid[row][el] == nil
        return true
      end
    end
    false
  end

  def full?
    lengths = 0
    @grid.each do |inner|
      lengths += inner.length
    end
    count == lengths
  end

  def place_random_ship
    raise "Error" if full?
    @grid[rand(0...@grid.length)][rand(0...@grid.length)] = :s
  end

  def won?
    count == 0
  end

end
