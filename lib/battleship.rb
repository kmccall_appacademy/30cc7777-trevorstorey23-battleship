require_relative "board"
require_relative "player"

class BattleshipGame
  attr_accessor :board, :player

  def initialize(player, board)
    @board = board
    @player = player
  end

  def attack(pos)
    puts "Hit!" if @board[pos] == :s
    puts "Miss" if @board[pos] == nil
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    pos = @player.get_play
    attack(pos)
  end

  def display_status
    puts "#{count} ships left"
    @board.grid
  end

  def play
    until game_over?
      play_turn
    end
    return "You won!"
  end

end
